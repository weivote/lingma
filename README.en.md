# asp公众号用通用领码发码随机抽奖系统

#### Description
用途举例：
随机领取（举例4-8）：
一个用户发送相同指令即时领取一条记录,被领取后其他人不可领取。
注意:关注用户无差别回复，注意识别一个用户多个号码关注等现象。
注意:领取用途请勿设置成随机回复：1个奖项会被回复给多人。
举例1 激活码随机发放:
一列是指令，第二列是游戏卡密或优惠券码等内容
发送指令随机回复指令对应的多条结果之一给发送者一人。
发送者再次发送相同指定回复之前内容，该条结果也不再回复其他人。
举例2：帐号密码随机发放
一列是指令，第二列是账号密码
发送指令随机回复指令对应的多条结果之一给发送者一人。
发送者再次发送相同指定回复之前内容，该条结果也不再回复其他人。
举例3：小型抽奖类用途:
以一等奖1个二等奖2个共20人抽奖为例：一共20行,一行一等奖,两行二等奖，其他行17行内容是[未中奖]
指令列一样，比如ling，前1-20人发送指令，每人随机得到一行(条)内容，之后其他人发送提示领取已结束。
举例4：小型秒抢用途:
以一等奖1个二等奖2个：一共3行,一行一等奖,两行二等奖
指令列一样，比如qiang，前3人随机得到一等奖二等奖，之后其他人发送提示结束；


#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)